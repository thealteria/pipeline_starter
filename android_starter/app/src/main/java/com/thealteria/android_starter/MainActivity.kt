package com.thealteria.android_starter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnHello: Button = findViewById(R.id.btnHello)

        btnHello.setOnClickListener {
            val intent = Intent(this, HelloWorldActivity::class.java)
            startActivity(intent)
        }
    }
}
package com.thealteria.android_starter

import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.plugins.GeneratedPluginRegistrant

class HelloWorldActivity : AppCompatActivity() {
    companion object {
        const val TAG = "HelloWorldActivity"
        const val FLUTTER_ENGINE_ID = "1"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello_world)

        prewarmFlutterEngine()

        val btnHello: Button = findViewById(R.id.btnHello)

        btnHello.setOnClickListener {
            startActivity(
                FlutterActivity
                    .withCachedEngine(FLUTTER_ENGINE_ID)
                    .build(this)
            )
        }
    }

    override fun onDestroy() {
        FlutterEngineCache.getInstance().get(FLUTTER_ENGINE_ID)?.destroy()
        FlutterEngineCache.getInstance().remove(FLUTTER_ENGINE_ID)
        super.onDestroy()
    }

    private fun prewarmFlutterEngine() {
        if (!FlutterEngineCache.getInstance().contains(FLUTTER_ENGINE_ID)) {
            Log.d(TAG, "prewarmFlutterEngine: warming up engine")

            val flutterEngine = FlutterEngine(this)
            GeneratedPluginRegistrant.registerWith(flutterEngine)

            // Start executing Dart code to pre-warm the FlutterEngine.
            flutterEngine.dartExecutor.executeDartEntrypoint(
                DartExecutor.DartEntrypoint.createDefault()
            )

            // Cache the FlutterEngine to be used by FlutterActivity.
            FlutterEngineCache
                .getInstance()
                .put(FLUTTER_ENGINE_ID, flutterEngine)
        } else {
            Log.d(TAG, "prewarmFlutterEngine: engine already warmed up")
        }
    }
}